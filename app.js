import colors from 'colors';
import { inquirerMenu, listDeleteTask, confirm, showCheckList } from './helpers/inquirer.js';
import readInput from './helpers/readInput.js';
import pause from './helpers/pause.js';
import Tasks from './models/tasks.js';
import { saveDB, readDB } from './helpers/saveFile.js';

const main = async () => {
    let opt = '';
    let description = null;
    const tasks = new Tasks();

    const tasksDB = readDB();

    if ( tasksDB ) 
        tasks.uploadTasksFromArray( tasksDB );

    do {
        opt =  await inquirerMenu();
        
        if ( opt === '1' ) {
            description = await readInput( 'Description:' );
            tasks.createTask( description );
        } else if ( opt === '2' )
            tasks.listComplete();
        else if( opt === '3' )
            tasks.listPendingCompleteTasks( 'complete' );
        else if( opt === '4' )
            tasks.listPendingCompleteTasks( 'pending' );
        else if( opt === '5' ) {
            const ids = await showCheckList( tasks.listArr );
            tasks.toggleCompleted( ids );
        } else if( opt === '6' ) {
            let id = await listDeleteTask( tasks.listArr );
           
            if ( id !== '0') {
                const ok = await confirm('you´re sure?')

                if ( ok ) {
                    tasks.deleteTask( id );
                    console.log('Task erease');
                } // end if
            } // end if
        }
       
        saveDB( tasks.listArr );

        if ( opt !== '0' ){ 
            await pause(); 
        }

    } while ( opt !== '0' );
}

main();