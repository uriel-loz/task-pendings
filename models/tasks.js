import colors from 'colors';
import Task from './task.js';
class Tasks {
    _list = {};

    get listArr() {
        const list = [];

        Object.keys( this._list ).forEach( ( value ) => { list.push( this._list[ value ] ) } );

        return list;
    }

    constructor() {
        this._list = {};
    }

    uploadTasksFromArray( tasks = [] ) {
        tasks.forEach( task  => {
            this._list[ task.id ] = task;
        } );
    }

    createTask( desc = '' ) {
        const task = new Task( desc );
        this._list[ task.id ] = task;
    }

    deleteTask( id = '' ){
        if( this._list[ id ] ) 
            delete this._list[ id ]
        
    }

    listComplete () {
        let count = 1;

        this.listArr.forEach( task => {
            let taskFormat = `${count.toString().green}. ${task.description} :: ${task.completed ? 'Complete'.green : 'Pending'.red}`;

            console.log(taskFormat);
        });
    }

    listPendingCompleteTasks = ( status ) => {
        let count = 1;
        let tasks = [];

        if ( status === 'complete' ) 
            tasks = this.listArr.filter( task => task.completed != null )
        else 
            tasks = this.listArr.filter( task => task.completed == null )

        tasks.forEach( task => {
            let taskFormat = `${count.toString().green}. ${task.description} :: ${task.completed.green}`;

            console.log(taskFormat);
        });
    }

    toggleCompleted( ids = [] ) {
        ids.forEach( id => {
            const task = this._list[ id ];

            if ( !task.completed  ) {
                task.completed = new Date().toISOString();
            }
        } );

        this.listArr.forEach( task => {
            if ( !ids.includes( task.id ) ) {
                this._list[ task.id ].completed = null;
            }
        } );
    }
}

export default Tasks;