import { v4 as uudiv4 } from 'uuid';

class Task {
    id = '';
    description = '';
    completed = null;

    constructor( description ) {
        this.id = uudiv4();
        this.description = description;
        this.completed = null;
    }
}


export default Task;