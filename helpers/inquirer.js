import inquirer from 'inquirer';
import colors from 'colors';

const questions = [
    {
        type: 'list',
        name: 'option',
        message: 'What do you want to do?',
        choices: [
            {
                value: '1',
                name: `${'1.'.green} Create task`
            },
            {
                value: '2',
                name: `${'2.'.green} List tasks`
            },
            {
                value: '3',
                name: `${'3.'.green} List completed tasks`
            },
            {
                value: '4',
                name: `${'4.'.green} List pending tasks`
            },
            {
                value: '5',
                name: `${'5.'.green} Complete task(s)`
            },
            {
                value: '6',
                name: `${'6.'.green} Delete task`
            },
            {
                value: '0',
                name: `${'0.'.green} Exit`
            }
        ]
    }
];

export const inquirerMenu = async () => {
    console.clear();
    console.log('=========================='.green);
    console.log('  Select an option'.white);
    console.log('==========================\n'.green);

    const { option } = await inquirer.prompt(questions);

    return option;
}

export const listDeleteTask = async( tasks = [] ) => {
    const choices = tasks.map( ( task, i ) => {
        let index = `${i + 1}.`.green;

        return {
            value: task.id,
            name: `${index} ${task.description}`,
        }
    } );

    choices.unshift(
        {
            value: '0',
            name: '0.'.green + ' Cancel',
        }
    );

    const questions = [
        {
            type: 'list',
            name: 'id',
            message: 'Erease',
            choices
        }
    ];

    const { id } = await inquirer.prompt(questions);

    return id;
}

export const showCheckList = async( tasks = [] ) => {
    const choices = tasks.map( ( task, i ) => {
        let index = `${i + 1}.`.green;

        return {
            value: task.id,
            name: `${index} ${task.description}`,
            checked: task.completed ? true : false
        }
    } );

    const question = [
        {
            type: 'checkbox',
            name: 'ids',
            message: 'Selections',
            choices
        }
    ];

    const { ids } = await inquirer.prompt(question);

    return ids;
}

export const confirm = async( message ) => {
    const question = [
        {
            type: 'confirm',
            name: 'ok',
            message
        }
    ];

    const { ok } = await inquirer.prompt(question);

    return ok;
}