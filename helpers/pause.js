import inquirer from 'inquirer';
import colors from 'colors';

const question = [
    {
        type: 'input',
        name: 'enter',
        message: `Press ${'ENTER'.green} to continue`
    }
];

const pause = async () => {

    console.log('\n');
    await inquirer.prompt(question);
}

export default pause;